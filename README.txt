Node2zip
=========
Node2zip is module that export HTML of single node.

Description
===========
It provides a "Tab" on the node detail page of configured content type. This module leverages the functionality of HTML Export module and behaves nearly identically. Through this module we can export the HTML of any particular node.

Dependencies
============
 * HTML Export module

Install
=======
1) Copy the Node2zip folder to the modules folder in your installation.
2) Enable the module using Administer -> Site building -> Modules
   (/admin/build/modules).
3) After installing this module you will find the "HTML Export" setting in
   additional setting of you content type. From there you can configure the
   content type by check box.
4) Now you will find the tab "Export" on your detail pages.

Permissions
==========
Give the permission for "export node Node2zip" to user before using the module.

Usage
===
1) Before using this module check that HTML Export is working properly.
2) When you click on the tab "Export" you will be redirect to the export package option page.
3) There will be three options (Zip, Tar, and Leave in files folder) for exporting the package.
4) After choosing an option from the select list and clicking the "Export" button HTML will start to export.
5) After successful exporting of HTML you will find an option to download the package.

Authors/maintainers
===================
Daffodil Software Limited - http://www.daffodilsw.com/
